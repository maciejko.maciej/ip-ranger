package om.mm.ipranger

import com.mm.ipranger.IPv4
import org.scalatest.flatspec.AnyFlatSpec

class IPv4Spec extends AnyFlatSpec {

  private val sampleIpWithValues = Map("127.0.0.1" -> 2130706433L,
      "208.80.152.2" -> 3494942722L,
      "10.255.255.255" -> 184549375L,
      "0.0.0.0" -> 0L,
      "255.255.255.255" -> 4294967295L,
      " 255.255.255.255  " -> 4294967295L   //with some spaces
    )

  private val improperIp = Seq("-1.0.0.0", "255.255.255.256", "1.1.1", "0.0.0.0.0")

  "Ipv4" should "parse sample address" in {
    sampleIpWithValues.foreach{ case (ipStr, expectedValue) =>
      val ip = IPv4(ipStr)
      assert(ip.value == expectedValue, s"Wrong conversion of ip $ipStr $ip")
    }
  }

  "Ipv4" should "convert to human readable state" in {
    sampleIpWithValues.foreach{ case (ipExpected, value) =>
      val ip = IPv4(value)
      assert(ip.toString == ipExpected.trim, s"Wrong conversion of value $value $ip")
    }
  }

  "Ipv4" should "fail when limit exceed" in {
    improperIp.foreach( badIp => assertThrows[IllegalArgumentException](IPv4(badIp)))
  }
}
