package om.mm.ipranger

import com.mm.ipranger.IpRange
import org.scalatest.funsuite.AnyFunSuite

class IpRangeSuite extends AnyFunSuite {

  private val mainRange = IpRange("10.15.3.155", "10.15.3.205")

  test("Construct range with incorrect from greater than to") {
    assertThrows[IllegalArgumentException] (
      IpRange("192.168.44.5", "192.168.44.1")
    )
  }

  test("Subtraction of range with no intersection") {
    val range = IpRange("10.15.4.155", "10.15.4.205")

    val result = mainRange - range

    assertResult(Seq(mainRange))(result)
  }

  test("Subtraction of range middle part") {
    val range = IpRange("10.15.3.200", "10.15.3.202")
    val expected = Seq(IpRange("10.15.3.155", "10.15.3.199"),
      IpRange("10.15.3.203", "10.15.3.205"))

    val result = mainRange - range

    assertResult(expected)(result)
  }

  test("Subtraction of range from the beginning") {
    val range = IpRange("10.15.3.100", "10.15.3.199")

    val result = mainRange - range

    assertResult(Seq(IpRange("10.15.3.200", "10.15.3.205")))(result)
  }

  test("Subtraction of range from the end") {
    val range = IpRange("10.15.3.200", "10.15.3.205")

    val result = mainRange - range

    assertResult(Seq(IpRange("10.15.3.155", "10.15.3.199")))(result)
  }

  test("Subtraction of whole range") {
    val range = IpRange("10.15.3.150", "10.15.3.210")

    val result = mainRange - range

    assertResult(Seq.empty)(result)
  }
}
