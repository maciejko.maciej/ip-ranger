package om.mm.ipranger

import com.mm.ipranger.IpRangeCalculatorJob
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession

/**
 * Requires es on localhost:9200
 */
object LiveDemoApp extends App with Logging {

  require(args.length == 2, "Run app with 2 arguments: input_file_path index_name")
  val inputFilePath = args(0)
  val indexName = args(1)

  logInfo(s"Input: $inputFilePath\nIndex name: $indexName")

  val spark = SparkSession.builder()
    .master("local[*]")
    .config("spark.es.port", 9200)
    .config("spark.es.nodes", "localhost")
    .config("spark.es.nodes.wan.only", "true")      //for docker
    .getOrCreate()

  new IpRangeCalculatorJob(spark).execute(inputFilePath, indexName)

  spark.stop()
}
