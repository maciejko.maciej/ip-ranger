package com.mm.ipranger.gen

import java.lang.Math.{abs, max, min}

import com.mm.ipranger.{IPv4, IpRange}

import scala.util.Random

private[gen] class IpRangeGenerator(tiny: Int, small: Int, medium: Int, huge: Int) {

  require(tiny >= 0 && small >= 0 && medium >= 0 && huge >= 0, "Weights have to be greater or equal 0")
  require(tiny.longValue + small + medium + huge <= Int.MaxValue, "Sum of weights has to be int value")
  require(tiny + small + medium + huge > 0, "Sum of weight has to be positive value")

  private val rand = new Random
  private val weightsSum = tiny + small + medium + huge
  private val smallThreshold = tiny + small
  private val mediumThreshold = smallThreshold + medium
  private val tinyDistance = 1L << 8
  private val smallDistance = 1L << 16
  private val mediumDistance = 1L << 24

  def genIpRange: IpRange = {
    val maxDistance = {
      val genVal = rand.nextInt(weightsSum)
      if (genVal < tiny) tinyDistance
      else if (genVal < smallThreshold) smallDistance
      else if (genVal < mediumThreshold) mediumDistance
      else IPv4.MaxValue
    }

    val a = abs(rand.nextLong()) % IPv4.MaxValue
    val b = abs(rand.nextLong()) % maxDistance
    val minAB = min(a,b)
    val maxAB = max(a,b)
    IpRange(maxAB-minAB, maxAB)
  }
}
