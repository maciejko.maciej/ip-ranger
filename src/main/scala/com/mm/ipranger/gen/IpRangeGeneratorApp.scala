package com.mm.ipranger.gen

import java.io.FileWriter

import com.mm.ipranger.utils.TryWithResources.withResources

object IpRangeGeneratorApp extends App {

  require(args.length == 2, "Execute application with 2 arguments: output_file_path ranges_amount ")

  val outputPath = args(0)
  val amount = args(1).toInt
  val gen = new IpRangeGenerator(1000, 100, 10, 2)

  withResources(new FileWriter(outputPath)) { fw =>
    for (_ <- 1 to amount) {
      val range = gen.genIpRange
      fw.write(s"${range.from.toDecOctet}, ${range.to.toDecOctet}\n")
    }
  }

}

