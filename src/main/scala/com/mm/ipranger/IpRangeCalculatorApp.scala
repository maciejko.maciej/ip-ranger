package com.mm.ipranger

import org.apache.spark.sql.SparkSession

object IpRangeCalculatorApp extends App {

  require(args.length == 2, "Job requires 2 arguments: input_path index_name")
  val inputFilePath = args(0)
  val indexName = args(1)

  val spark = SparkSession.builder()
    .appName("Unique ip range calculator")
    .getOrCreate()
  new IpRangeCalculatorJob(spark).execute(inputFilePath, indexName)
}
