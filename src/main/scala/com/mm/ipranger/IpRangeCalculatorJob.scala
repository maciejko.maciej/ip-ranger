package com.mm.ipranger

import com.mm.ipranger.es.{IndexCreator, IpRangeEs}
import org.apache.spark.sql.{Row, SparkSession}
import org.elasticsearch.spark.sql._

import IPv4.longToIPv4

class IpRangeCalculatorJob(spark: SparkSession) extends Serializable {

  def execute(inputFilePath: String, indexName: String): Unit = {
    createIndex(indexName)

    import org.apache.spark.sql.functions._
    import spark.implicits._

    val inputData = spark.read.textFile(inputFilePath).map{ line =>
      val ipRange = line.split(",")
      require(ipRange.size == 2)
      IpRange(IPv4(ipRange(0)), IPv4(ipRange(1)))
    }.toDF().withColumn("id", monotonically_increasing_id()).cache()

    val rangesWithExclusion = inputData.as("a")
      .join(inputData.as("b"),
        $"a.id".notEqual($"b.id")
          .and($"a.from.value".leq($"b.from.value").and($"a.to.value".geq($"b.from.value"))
            .or($"b.from.value".leq($"a.from.value").and($"b.to.value".geq($"a.from.value")))), "left")
      .groupBy($"a.from.value", $"a.to.value", $"a.id")
      .agg(collect_list($"b.from.value").as("from.aggr"), collect_list($"b.to.value").as("to.aggr"))

    def excludeRanges(row: Row): Seq[IpRange] = {
      val rangeFrom = row.getLong(0)
      val rangeTo = row.getLong(1)
      val aggrFrom = row.getSeq[Long](3)
      val aggrTo = row.getSeq[Long](4)
      val exclusions = aggrFrom.zip(aggrTo).map{case (a,b) => IpRange(a, b)}
      IpRange(rangeFrom, rangeTo).exclude(exclusions)
    }

    rangesWithExclusion
      .rdd
      .flatMap(excludeRanges)
      .map(IpRangeEs(_))
      .toDF()
      .saveToEs(indexName)
  }

  private def createIndex(indexName: String): Unit = {
    val response = new IndexCreator(spark.conf.get("spark.es.nodes"), spark.conf.get("spark.es.port").toInt)
      .putWithMapping(indexName, IpRangeEs.esFieldTypes)
    if (!response.isSuccess)
      throw new RuntimeException(s"Create index failed ${response.code} ${response.body}")
  }
}
