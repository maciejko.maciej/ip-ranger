package com.mm.ipranger

import com.mm.ipranger.IPv4.{max, min}

case class IpRange(from: IPv4, to: IPv4) {
  require(from.value <= to.value, s"Can't construct range [${from.toDecOctet}, ${to.toDecOctet}]")

  override def toString: String = s"[$from, $to]"

  def -(that: IpRange): Seq[IpRange] =
    if (min(to, that.to) < max(from, that.from)) {      //out of range
      Seq(this)
    } else if (from >= that.from && to <= that.to) {    //subtract whole
      Seq.empty
    } else if (from <= that.from && to >= that.to) {    //in the middle
      val a = if (from != that.from) Some(IpRange(from, that.from - 1)) else None
      val b = if (that.to != to) Some(IpRange(that.to + 1, to)) else None
      Seq(a, b).flatten
    } else if (from < that.from) {          //subtract from the end
      Seq(IpRange(from, that.from - 1))
    } else {                                //subtract from the beginning
      Seq(IpRange(that.to + 1, to))
    }

  def exclude(exclusion: Seq[IpRange]): Seq[IpRange] =
    exclusion.foldLeft(Seq(this)) { (acc, exclude) => acc.flatMap(_ - exclude) }
}