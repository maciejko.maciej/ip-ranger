package com.mm.ipranger.utils

object TryWithResources {

  type Closeable = {
    def close(): Unit
  }

  def withResources[T <: Closeable, V](r: => T)(f: T => V): V = {
    val resource = r
    try {
      f(resource)
    } finally {
      resource.close()
    }
  }

}
