package com.mm.ipranger

import java.util.regex.Pattern

case class IPv4(value: Long) {

  require(value >= IPv4.MinValue && value <= IPv4.MaxValue, s"Value $value is not proper IP address value")

  def toDecOctet: String = {
    val o4 = value % 256
    val o3 = (value >> 8) % 256
    val o2 = (value >> 16) % 256
    val o1 = (value >> 24) % 256
    s"$o1.$o2.$o3.$o4"
  }

  protected[ipranger] def +(inc: Long): IPv4 = IPv4(value + inc)
  protected[ipranger] def -(dec: Long): IPv4 = IPv4(value - dec)
  protected[ipranger] def <(that: IPv4): Boolean = value < that.value
  protected[ipranger] def <=(that: IPv4): Boolean = value <= that.value
  protected[ipranger] def >=(that: IPv4): Boolean = value >= that.value
  protected[ipranger] def >(that: IPv4): Boolean = value > that.value

  override def toString: String = toDecOctet
}


object IPv4 {
  val MaxValue = 1L << 32
  val MinValue = 0L

  private val dotPattern = Pattern.quote(".")

  def apply(decOctect: String): IPv4 = {
    val octects = decOctect.trim.split(dotPattern)
    require(octects.size == 4, s"Improper ip address format $decOctect")

    val octestsNum = octects.map(_.toLong)
    octestsNum.foreach(x => require(x >= 0 && x < 256, s"Improper octets range for $decOctect"))

    val value = octestsNum(0) << 24 | octestsNum(1) << 16 | octestsNum(2) << 8 | octestsNum(3)
    IPv4(value)
  }

  implicit def longToIPv4(value: Long): IPv4 = IPv4(value)
  implicit def stringToIPv4(value: String): IPv4 = IPv4(value)

  def min(a: IPv4, b: IPv4): IPv4 = if (a<b) a else b
  def max(a: IPv4, b: IPv4): IPv4 = if (a<b) b else a
}
