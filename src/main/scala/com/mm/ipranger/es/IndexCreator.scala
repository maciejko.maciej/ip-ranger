package com.mm.ipranger.es

import scalaj.http.{Http, HttpOptions, HttpResponse}

import scala.util.parsing.json.JSONObject

class IndexCreator(host: String, port: Int, readTimeoutMs: Int = 10000) {

  def putWithMapping(indexName: String, filedTypes: Map[String, String]): HttpResponse[String] = {
    val content = {
      val fieldTypesJson = JSONObject(filedTypes.mapValues(fieldType => JSONObject(Map("type" -> fieldType))))
      JSONObject(Map("mappings" ->
        JSONObject(Map("properties" -> fieldTypesJson))))
    }.toString()

    Http(s"http://$host:$port/$indexName")
      .put(content)
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(readTimeoutMs))
      .asString
  }

}
