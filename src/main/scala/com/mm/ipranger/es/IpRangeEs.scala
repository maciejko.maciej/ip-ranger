package com.mm.ipranger.es

import com.mm.ipranger.IpRange

case class IpRangeEs(from: String, to: String)

object IpRangeEs {
  def apply(source: IpRange): IpRangeEs = IpRangeEs(source.from.toDecOctet, source.to.toDecOctet)

  val esFieldTypes: Map[String, String] =  Map("from" -> "ip", "to" -> "ip")
}
