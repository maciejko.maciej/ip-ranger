# Unique IP ranges calculator #
Spark application for calculation unique ip ranges from given set.
Job reads input file with ip ranges like:
```
197.203.0.0, 197.206.9.255
197.204.0.0,197.204.0.24
201.233.7.160, 201.233.7.168
201.233.7.164, 201.233.7.168
201.233.7.167, 201.233.7.167
203.133.0.0, 203.133.255.255
```
remove intersections and return set of mutually exclusive ip ranges. Output will be stored in ElasticSearch.
Spark job will for sample above will store results in ES like:
```
197.203.0.0, 197.203.255.255
197.204.0.25, 197.206.9.255
201.233.7.160, 201.233.7.163
203.133.0.0, 203.133.255.255
```
## Live demo ##
Start elasticsearch locally on port 9200:

```
cd docker
docker-compose up
```
Run test application ```om.mm.ipranger.LiveDemoApp``` with 2 arguments: input_file_path index_name. You can use sample from ```test/resources/sample_input.txt```
Demo app will create new ES index (based on 2. argument), start spark as local, process input file and store results in ES.
Notice that job fails when index already exists.

### Ip ranges generator ###
Run application ```com.mm.ipranger.gen.IpRangeGeneratorApp``` with 2 arguments: output_file_path number_of_ranges.

## Build fat jar ##
Just run ```sbt assembly```

## Run on spark standalone cluster ##
Build fat jar.
```
    ./bin/spark-submit
        --master <master-url>
        --class com.mm.ipranger.IpRangeCalculatorApp
        --conf spark.es.port=<es.port>
        --conf spark.es.nodes=<es.host>
        --conf ...
        ip-ranger_1.0.jar <input_file_path> <index_name>
```

