name := "ip-ranger"

version := "0.1"

scalaVersion := "2.11.12"

def sparkVersion = "2.4.7"
def elasticVersion = "7.10.1"

libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion % Provided
libraryDependencies += "org.apache.spark" %% "spark-streaming" % sparkVersion % Provided
libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion % Provided
libraryDependencies += "org.elasticsearch" %% "elasticsearch-spark-20" % elasticVersion
libraryDependencies += "org.scalaj" %% "scalaj-http" % "2.4.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.3" % Test
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.15.1" % Test

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
mainClass in assembly := Some("com.mm.ipranger.IpRangeCalculatorApp")
assemblyJarName in assembly := "ip-ranger_1.0.jar"